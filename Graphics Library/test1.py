try:
    import Tkinter as tk
except (ModuleNotFoundError):
    import tkinter as tk

import tktouch as tkt

root = tk.Tk()
main_window = tkt.Window(root)
frame1 = tkt.Frame(main_window)
main_window.after(2000, lambda: main_window.configure_appearance(application_background_color="#FFCCFF"))
test_scheme = {"application_background_color": "#CCFFFF",
               "application_border_color": "#00DD00",
               "application_border_width": 5}
main_window.after(4000, lambda: main_window.config(appearance_scheme=test_scheme, full_screen=True))
main_window.after(6000, lambda: main_window.config(full_screen=False))

root.mainloop()
