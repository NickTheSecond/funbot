try:
    import Tkinter as tk
except ModuleNotFoundError:
    import tkinter as tk
from tktouch._default_appearance import default_scheme

class Window(tk.Frame):
    """
    Used as the topmost GUI layer, should be given a tk_parent such as root.
    Houses a single tk.Frame, which contains a tk.Canvas
    All children are placed within the canvas
    """

    # the default appearance scheme
    default_scheme = default_scheme

    # the default configuration kwargs
    default_config = {"appearance_scheme": {},  # passes a scheme dictionary to easily change colors and layouts
                      "full_screen": False,  # enables full-screen applications
                      "use_window_manager": True,  # replaces the default window manager when False
                      "windowed_dims": [1024, 720, 100, 100],  # window geometry (width, height, xpos, ypos)
                      "min_dims": [640, 480],  # min window geometry (width, height)
                      "max_dims": [1200, 0],  # max window geometry (width, height) Disable with value <= min_dims
                      }

    # constructs a Window object, must have a tk_parent
    def __init__(self, tk_parent, **kwargs):
        self.tk_parent = tk_parent
        tk.Frame.__init__(self, self.tk_parent)
        self.pack(fill="both", expand=True)

        # create canvas to house children
        self.canvas = tk.Canvas(self, highlightthickness=0)
        self.canvas.pack(fill="both", expand=True)

        # deploy the default appearance scheme
        self.scheme = {}
        self.reset_appearance()

        # deploy the default configuration
        self.configuration = {}
        self.reset_config()

        # configure self according to kwargs
        self.configuration.update(**kwargs)
        self.config(**self.configuration)
        self.config_appearance()

    # configures the Window object
    def configure(self, *args, **kwargs):
        self.configuration.update(**kwargs)
        if "appearance_scheme" in kwargs.keys():
            self.config_appearance(**self.configuration["appearance_scheme"])
        if "full_screen" in kwargs.keys():
            self.tk_parent.overrideredirect(self.configuration["full_screen"] or not self.configuration["use_window_manager"])
            if self.configuration["full_screen"]:
                self.tk_parent.unbind("<Configure>")
                self.tk_parent.maxsize(0,0)
                self.tk_parent.geometry("{0}x{1}+0+0".format(self.tk_parent.winfo_screenwidth(),
                                                             self.tk_parent.winfo_screenheight()))
            else:
                def update_windowed_geometry(event):
                    self.configuration["windowed_dims"][0] = max(event.width, self.configuration["min_dims"][0])
                    if self.configuration["max_dims"][0] > self.configuration["min_dims"][0]:
                        self.configuration["windowed_dims"][0] = min(self.configuration["windowed_dims"][0],
                                                                     self.configuration["max_dims"][0])
                    self.configuration["windowed_dims"][1] = max(event.height, self.configuration["min_dims"][1])
                    if self.configuration["max_dims"][1] > self.configuration["min_dims"][1]:
                        self.configuration["windowed_dims"][1] = min(self.configuration["windowed_dims"][1],
                                                                     self.configuration["max_dims"][1])
                    self.configuration["windowed_dims"][2:3] = [self.tk_parent.winfo_x(), self.tk_parent.winfo_y()]
                self.tk_parent.bind("<Configure>", update_windowed_geometry)
                self.tk_parent.maxsize(*self.configuration["max_dims"])
                self.tk_parent.geometry("{0}x{1}+{2}+{3}".format(*self.configuration["windowed_dims"]))
        if "use_window_manager" in kwargs.keys():
            self.tk_parent.overrideredirect(self.configuration["full_screen"] or not self.configuration["use_window_manager"])
        if "min_dims" in kwargs.keys():
            self.tk_parent.minsize(*self.configuration["min_dims"])
        if "max_dims" in kwargs.keys():
            self.tk_parent.maxsize(*self.configuration["max_dims"])
        for kwarg in kwargs.keys():
            if kwarg not in self.default_config:
                tk.Frame.config(self, {kwarg: kwargs[kwarg]} )
    config = configure

    # configures the appearance of the Window object
    def configure_appearance(self, *args, **kwargs):
        self.scheme.update(**kwargs)
        if self.scheme["application_border_color"] != self["bg"]:
            self.config(bg=self.scheme["application_border_color"])
        if self.scheme["application_border_width"] != self["padx"] or self["padx"] != self["pady"]:
            self.config(padx=self.scheme["application_border_width"], pady=self.scheme["application_border_width"])
        if self.scheme["application_background_color"] != self.canvas["bg"]:
            self.canvas.config(bg=self.scheme["application_background_color"])
    config_appearance = configure_appearance

    # resets the appearance scheme to the default
    def reset_appearance(self, *args):
        self.scheme = self.default_scheme.copy()

    # resets the configuration to the default
    def reset_configuration(self, *args):
        self.configuration = self.default_config.copy()
    reset_config = reset_configuration
