try:
    import Tkinter as tk
except ModuleNotFoundError:
    import tkinter as tk
from tktouch._default_appearance import default_scheme

class Frame(tk.Frame):
    """
    Used as the basic GUI layer, should be given a tktouch.Frame or tktouch.Window as parent.
    Houses a single tk.Frame, which contains a tk.Canvas
    All children are placed within the canvas
    """

    # the default appearance scheme
    default_scheme = default_scheme

    # the default configuration kwargs
    default_config = {"appearance_scheme": {},  # passes a scheme dictionary to easily change colors and layouts
                      "dims": [100, 100, 20, 20],  # widget geometry (width, height, xpos, ypos)
                      "min_dims": [10, 10],  # min widget geometry (width, height)
                      "max_dims": [1200, 0],  # max widget geometry (width, height) Disable with value <= min_dims
                      }

    # constructs a Frame object, must have a tk_parent
    def __init__(self, tktouch_parent, **kwargs):
        self.tktouch_parent = tktouch_parent
        self.tk_parent = self.tktouch_parent.canvas
        self.id = tk.Frame.__init__(self, self.tk_parent)

        # create canvas to house children
        self.canvas = tk.Canvas(self, highlightthickness=0)
        self.canvas.pack(fill="both", expand=True)

        # deploy the default appearance scheme
        self.scheme = {}
        self.reset_appearance()

        # deploy the default configuration
        self.configuration = {}
        self.reset_config()

        # configure self according to kwargs
        self.configuration.update(**kwargs)
        self.config(**self.configuration)
        self.config_appearance()

    # configures the Window object
    def configure(self, *args, **kwargs):
        self.configuration.update(**kwargs)
        if "appearance_scheme" in kwargs.keys():
            self.config_appearance(**self.configuration["appearance_scheme"])
        if "dims" in kwargs.keys():
            self.configuration["dims"] = [
                max(self.configuration["dims"][0], self.configuration["min_dims"][0]),
                min(self.configuration["dims"][1], self.configuration["max_dims"][1])
            ]
            tk.Frame.config(self, width=self.configuration["dims"][0], height=self.configuration["dims"][1])
        # if "min_dims" in kwargs.keys():
        #     self.configuration["min_dims"] = [
        #         max(self.configuration["min_dims"])
        #     ]
        # if "max_dims" in kwargs.keys():
        #    tk.Frame.config(self, width=self.configuration["max_dims"])
        for kwarg in kwargs.keys():
            if kwarg not in self.default_config:
                tk.Frame.config(self, {kwarg: kwargs[kwarg]})
    config = configure

    # configures the appearance of the Window object
    def configure_appearance(self, *args, **kwargs):
        self.scheme.update(**kwargs)
        if self.scheme["application_border_color"] != self["bg"]:
            self.config(bg=self.scheme["application_border_color"])
        if self.scheme["application_border_width"] != self["padx"] or self["padx"] != self["pady"]:
            self.config(padx=self.scheme["application_border_width"], pady=self.scheme["application_border_width"])
        if self.scheme["application_background_color"] != self.canvas["bg"]:
            self.canvas.config(bg=self.scheme["application_background_color"])
    config_appearance = configure_appearance

    # resets the appearance scheme to the default
    def reset_appearance(self, *args):
        self.scheme = self.default_scheme.copy()

    # resets the configuration to the default
    def reset_configuration(self, *args):
        self.configuration = self.default_config.copy()
    reset_config = reset_configuration
