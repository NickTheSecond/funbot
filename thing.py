#!/usr/bin/env python

'''
Created on Nov 5, 2019

@author: Travis Rogers
'''

# hello there

import sys
import time
import csv
from tkinter.constants import CURRENT
from multiprocessing import Process, Manager

try:
    from PIL import Image, ImageTk
except:
    print("This program requires pillow installed!")
    print("Install pillow from 'https://www.lfd.uci.edu/~gohlke/pythonlibs/#pillow' via pip")
    raise (ImportError())

# because developing the GUI on a windows machine is much easier, lets make sure the program can differentiate between the pi and use a compatability mode with no motor control functionality when not on a rasPi
try:
    import RPi.GPIO as GPIO

    running_on_pi = True
except:
    running_on_pi = False

# because the tkinter library is version dependant, lets import it according to python version
if sys.version_info[0] == 2:
    # script is running in a python 2.X environment, use old Tkinter library
    import Tkinter as tk
elif sys.version_info[0] == 3:
    # script is running in a python 3.X environment, use new tkinter library
    import tkinter as tk

# TODO: add documentation
class Encoder:
    def __init__(self, pin1, pin2):
        self.pin1 = pin1
        self.pin2 = pin2
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(pin1, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(pin2, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

        manager = Manager()
        self.data = manager.dict()
        self.data['magnitude'] = 0
        self.data['speed'] = 0

        self.process = Process(target=self.track, args=(self.data,))
        self.process.start()

    def getPosition(self):
        return self.data['magnitude']

    def getSpeed(self):
        return self.data['speed']

    def track(self, data):
        averageTime = 0.25
        history = []
        positionData = [[time.time(), 0]]
        last = [GPIO.input(27), GPIO.input(17)]
        while True:
            current = [GPIO.input(27), GPIO.input(17)]
            if len(history) < 2:
                history.append(current)
            else:
                history = [history[1], current]
                if history[0] == history[1]:
                    if last != current:
                        last = current
                        data['magnitude'] += 1/2 # each is half a degree
                        positionData.append([time.time(), data['magnitude']])
                 
                        for i in range(len(positionData)-1, -1, -1):
                            if time.time() > positionData[i][0]+averageTime:
                                positionData = positionData[i+1:]
                                break
                        if len(positionData) > 0:
                            rate = (data['magnitude']-positionData[0][1])/(time.time()-positionData[0][0])
                            data['speed'] = rate

# Imaging with tkinter and PIL can be convoluted, so lets stick some useful features in a class to make image handling on a canvas easier down the line
class ImageVar():
    def __init__(self, image_path="", pix_width=0, pix_height=0):

        # Store dimensions as IntVars so changes can be traced
        self.pix_width = tk.IntVar()
        self.pix_width.set(pix_width)
        self.pix_width.trace("w", self.update_dim)
        self.pix_height = tk.IntVar()
        self.pix_height.set(pix_height)
        self.pix_height.trace("w", self.update_dim)

        # Store image path as a StringVar so changes can be traced
        self.image_path = tk.StringVar()
        self.set(image_path)

    # Bound to a change to pix_width or pix_height so any change will automatically resize the pil_image
    def update_dim(self, *args):
        if self.pix_width.get() != 0 and self.pix_height.get() != 0:
            self.pil_resized = self.pil_image.resize((self.pix_width.get(), self.pix_height.get()), Image.ANTIALIAS)
        else:
            # if picture is 0 wide or 0 tall, use the original image dimensions
            self.pil_resized = self.pil_image

    # a tkinter-Var style setter for describing the file path to an image to be loaded
    def set(self, image_path):
        self.image_path.set(image_path)
        self.pil_image = Image.open(image_path)
        self.update_dim()

    # a tkinter-Var style trace for providing callback functionality when an object is changed
    def trace(self, mode, callback):
        self.image_path.trace(mode, callback)
        self.pix_width.trace(mode, callback)
        self.pix_height.trace(mode, callback)

    # a tkinter-Var style getter for retrieving a processed photo_image from the object, used for displaying in tkinter widget or canvas element
    def get(self):
        self._image_backup = ImageTk.PhotoImage(self.pil_resized)
        return (self._image_backup)


# A class which consolidates the repetetive code of displaying an image on a canvas, and handling mouse events for that image
class action_tile():
    def __init__(self, parent_canvas, image="Resources/Tile_Placeholder.png", xpos=0, ypos=0, width=48, enabled=True,
                 no_tile=False):

        # Save the parent_canvas object to be referenced in all GUI events
        self.parent = parent_canvas

        # define class variables
        self._no_tile = no_tile
        self._mouse_on_click = 0, 0
        self._floating = False
        self.type = "null"
        self.selected = False

        # make element position, width, and command variables all IntVars, so they can be traced when changed
        self.x = tk.IntVar()
        self.x.set(xpos)
        self.x.trace("w", self.update_pos)
        self.y = tk.IntVar()
        self.y.set(ypos)
        self.y.trace("w", self.update_pos)

        self.width = tk.IntVar()
        self.width.set(width)

        # the motor controls to set after executing this tile as a drive tile during command runtime
        self.left_speed = tk.IntVar()
        self.left_speed.set(75)
        self.right_speed = tk.IntVar()
        self.right_speed.set(75)

        # the face to display after executing this tile as a face tile during command runtime
        self.face = tk.StringVar()
        self.face.set("Happy")
        self.face.trace("w", self.update_face_icons)

        # the time to wait after executing this tile as a wait or drive tile during command runtime
        self.wait = tk.IntVar()  # in millis
        self.wait.set(1500)

        # Create a background for the tile, display it, and bind mouse events to it
        if not self._no_tile:
            self.tile_image = ImageVar(image_path="Resources/Tile.png", pix_width=self.width.get(),
                                       pix_height=self.width.get())
            self.tile_id = self.parent.create_image((self.x.get(), self.y.get()), image=self.tile_image.get())

        # Create an icon for the tile, display it, and bind mouse events to it
        self.icon_image = ImageVar(image_path=image, pix_width=self.width.get(), pix_height=self.width.get())
        self.icon_id = self.parent.create_image((self.x.get(), self.y.get()), image=self.icon_image.get())

        # part of the data collection code
        if enabled:
            self.bind_events()

        # commander the button press and release events for this tile from parent
        self.parent.tag_unbind(self.icon_id, "<Button-1>")
        self.parent.tag_unbind(self.icon_id, "<ButtonRelease-1>")
        self.parent.tag_bind(self.icon_id, "<Button-1>", self.on_click)
        self.parent.tag_bind(self.icon_id, "<ButtonRelease-1>", self.on_release)

    # an empty method which can be reassigned after object instantiation. Is when dropping a tile after a drag event
    def drop_command(self):
        pass

    # updates tile image to the selected face image file
    def update_face_icons(self, *args):
        self.update_icon("Resources/" + self.face.get() + ".png", self.width.get(), self.width.get())

    # updates tile image to a direction arrow image file suitable for the drive direction (this could be better tuned and more consisely writen but it works so why take the time?)
    def update_drive_icons(self, *args):
        difference = abs(self.right_speed.get() - self.left_speed.get())
        if self.right_speed.get() > 0:
            if difference > 100:
                self.update_icon("Resources/Turn_CCW.png", self.width.get(), self.width.get())
            elif difference >= 75:
                if self.left_speed.get() > self.right_speed.get():
                    self.update_icon("Resources/Forward_Hard_Right.png", self.width.get(), self.width.get())
                else:
                    self.update_icon("Resources/Forward_Hard_Left.png", self.width.get(), self.width.get())
            elif difference >= 25:
                if self.left_speed.get() > self.right_speed.get():
                    self.update_icon("Resources/Forward_Soft_Right.png", self.width.get(), self.width.get())
                else:
                    self.update_icon("Resources/Forward_Soft_Left.png", self.width.get(), self.width.get())
            else:
                self.update_icon("Resources/Forward_Straight.png", self.width.get(), self.width.get())
        if self.right_speed.get() < 0:
            if difference > 100:
                self.update_icon("Resources/Turn_CW.png", self.width.get(), self.width.get())
            elif difference >= 75:
                if self.left_speed.get() > self.right_speed.get():
                    self.update_icon("Resources/Backward_Hard_Right.png", self.width.get(), self.width.get())
                else:
                    self.update_icon("Resources/Backward_Hard_Left.png", self.width.get(), self.width.get())
            elif difference >= 25:
                if self.left_speed.get() > self.right_speed.get():
                    self.update_icon("Resources/Backward_Soft_Right.png", self.width.get(), self.width.get())
                else:
                    self.update_icon("Resources/Backward_Soft_Left.png", self.width.get(), self.width.get())
            else:
                self.update_icon("Resources/Backward_Straight.png", self.width.get(), self.width.get())
        if self.right_speed.get() == 0 and self.left_speed.get() == 0:
            self.update_icon("Resources/Stop.png", self.width.get(), self.width.get())

    # method which is called after releasing a button press. Binds the tkinter variables from the menu elements to this particular tile
    def release_command(self):
        if self.type == "drive":
            self.parent.master.master.drive_frame.tkraise()
            self.parent.master.master.drive_frame.right_slider.config(variable=self.right_speed)
            self.parent.master.master.drive_frame.left_slider.config(variable=self.left_speed)
            self.parent.master.master.drive_frame.wait_slider.config(variable=self.wait)
            self.right_speed.trace("w", self.update_drive_icons)
            self.left_speed.trace("w", self.update_drive_icons)
        elif self.type == "face":
            self.parent.master.master.face_frame.tkraise()
        elif self.type == "wait":
            self.parent.master.master.wait_frame.tkraise()
            self.parent.master.master.wait_frame.wait_slider.config(variable=self.wait)

    # binds to mouse events from parent
    def bind_events(self):
        if not self._no_tile:
            self.parent.tag_bind(self.tile_id, "<Button-1>", self.on_click)
            self.parent.tag_bind(self.tile_id, "<B1-Motion>", self.on_drag)
            self.parent.tag_bind(self.tile_id, "<ButtonRelease-1>", self.on_release)
        self.parent.tag_bind(self.icon_id, "<B1-Motion>", self.on_drag)

    # unbinds from mouse events from parent
    def unbind_events(self):
        if not self._no_tile:
            self.parent.tag_unbind(self.tile_id, "<Button-1>")
            self.parent.tag_unbind(self.tile_id, "<B1-Motion>")
            self.parent.tag_unbind(self.tile_id, "<ButtonRelease-1>")
        self.parent.tag_unbind(self.icon_id, "<B1-Motion>")

    # records the current mouse x,y for use in the drag animation
    def on_click(self, event, *args):
        self._mouse_on_click = event.x - self.x.get(), event.y - self.y.get()

    # an intermediatory method which calls release_command as well as calls the appropriate tile graphics update methods
    def on_click_release(self, *args):
        print("click_release")
        self.release_command()
        if self.type in ["drive", "face", "wait"]:
            for target_tile in self.parent.master.master.tile_list:
                target_tile.update_tile_image("Resources/Tile.png", target_tile.width.get(), target_tile.width.get())
                target_tile.selected = False
            self.update_tile_image("Resources/Selected_Tile.png", self.width.get(), self.width.get())
            self.selected = True
            self.parent.master.master.face_frame.face_var.set(self.face.get())

    # Uses current mouse position and recorded mouse start position to animate a dragged tile
    def on_drag(self, event, *args):
        self.x.set(event.x - self._mouse_on_click[0])
        self.y.set(event.y - self._mouse_on_click[1])
        self._floating = True

    # Determines if the mouse release was after a drag (aka a drop event) or a click (aka a click release) and calls according methods
    def on_release(self, event, *args):
        if self._floating:
            self._floating = False
            print("Drop")
            self.drop_command()
        else:
            self.on_click_release()

    # updates this tile's icon to the according image_path with the specified dimensions via the use of the ImageVar class
    def update_icon(self, image_path, pix_width=0, pix_height=0):
        self.icon_image.set(image_path)
        self.icon_image.pix_width.set(pix_width)
        self.icon_image.pix_height.set(pix_height)
        self.parent.itemconfig(self.icon_id, image=self.icon_image.get())

    # updates this tile's background image to the according image_path with the specified dimensions via the use of the ImageVar class
    def update_tile_image(self, image_path, pix_width=0, pix_height=0):
        self.tile_image.set(image_path)
        self.tile_image.pix_width.set(pix_width)
        self.tile_image.pix_height.set(pix_height)
        self.parent.itemconfig(self.tile_id, image=self.tile_image.get())

    # updates this tile's position on the canvas according to the set x,y coords
    def update_pos(self, *args):
        self.parent.coords(self.icon_id, (self.x.get(), self.y.get()))
        self.parent.coords(self.tile_id, (self.x.get(), self.y.get()))


# This class contains the majority of the main program, and is used to instanciate a GUI window element via tkinter
class main_window(tk.Frame):
    def __init__(self, master):

        # Inherit class tk.Frame, lets call tk.Frame.__init__ to initialize inherited class
        tk.Frame.__init__(self, master)
        self.pack(expand=True, fill=tk.BOTH)

        # Declare all new data members for class
        self.master = master

        self.after_list = []

        # Makes window full screen. Force redraw by simply updating self.pad or calling self.redraw()
        self.master.overrideredirect(True)
        self.master.geometry("480x320+0+0")

        # Lets define the grid geometry of the main window frame
        self.columnconfigure(0, minsize=160)
        self.columnconfigure(1, weight=1)
        self.rowconfigure(0, weight=1)

        # Create a frame  and Canvasfor the runtime GUI
        self.run_frame = tk.Frame(self)
        self.run_frame.grid(row=0, column=0, columnspan=2, sticky="NESW")

        self.run_canvas = tk.Canvas(self.run_frame, bg="light blue")
        self.run_canvas.pack(fill="both", expand=True)
        self.run_canvas.bind("<Button-1>", self.cancel_impending_commands)

        self.display_tile = action_tile(self.run_canvas, xpos=240, ypos=160, enabled=False)
        self.display_tile.update_icon("Resources/Happy.png", 0, 0)

        # Create a frame for the settings available to a drive tile
        self.drive_frame = tk.Frame(self, bg="light blue")
        self.drive_frame.grid(row=0, column=0, sticky="NESW")

        # the menus title
        self.drive_frame.label = tk.Label(self.drive_frame, text="Drive Command", font="18", bg="#73afff")
        self.drive_frame.label.pack(anchor="n", fill="x")

        # a label and slider for right motor speed in +-%
        self.drive_frame.right_label = tk.Label(self.drive_frame, text="\nRight Speed:", font="16", bg="light blue")
        self.drive_frame.right_label.pack(anchor="center")

        self.drive_frame.right_slider = tk.Scale(self.drive_frame, from_=-100, to=100, orient=tk.HORIZONTAL, width=30,
                                                 length=150, bg="light blue", resolution=25)
        self.drive_frame.right_slider.pack(anchor="center")

        # a label and slider for left motor speed in +-%
        self.drive_frame.left_label = tk.Label(self.drive_frame, text="Left Speed:", font="16", bg="light blue")
        self.drive_frame.left_label.pack(anchor="center")

        self.drive_frame.left_slider = tk.Scale(self.drive_frame, from_=-100, to=100, orient=tk.HORIZONTAL, width=30,
                                                length=150, bg="light blue", resolution=25)
        self.drive_frame.left_slider.pack(anchor="center")

        # a label and slider for drive duration in milliseconds
        self.drive_frame.wait_label = tk.Label(self.drive_frame, text="Duration:", font="16", bg="light blue")
        self.drive_frame.wait_label.pack(anchor="center")

        self.drive_frame.wait_slider = tk.Scale(self.drive_frame, from_=500, to=15000, orient=tk.HORIZONTAL, width=30,
                                                length=150, bg="light blue", resolution=500)
        self.drive_frame.wait_slider.pack(anchor="center")

        # Create a frame for the settings available to a face tile
        self.face_frame = tk.Frame(self, bg="#f6ff7d")
        self.face_frame.grid(row=0, column=0, sticky="NESW")

        # the menus title
        self.face_frame.label = tk.Label(self.face_frame, text="Face Command", font="18", bg="#ffff00")
        self.face_frame.label.pack(anchor="n", fill="x")

        # Create a label for the dropdown menu, and a drowdown menu with a list of faces to choose from
        self.face_frame.option_label = tk.Label(self.face_frame, text="\n\nPick a Face:", font="18", bg="#f6ff7d")
        self.face_frame.option_label.pack(anchor="center")
        face_options = ["Angry",
                        "Annoyed",
                        "Confused",
                        "Cute",
                        "Happy",
                        "Happy_Look_Left",
                        "Happy_Look_Right",
                        "Neutral",
                        "Sad",
                        "Silly",
                        "Surprised",
                        "Very_Angry",
                        "Very_Happy",
                        "Very_Sad"]
        self.face_frame.face_var = tk.StringVar()
        self.face_frame.drop_down = tk.OptionMenu(self.face_frame, self.face_frame.face_var, *face_options)
        self.face_frame.drop_down.pack(anchor="center", fill="x")

        def face_var_trace(*args):
            for tile in self.tile_list:
                if tile.selected:
                    if tile.type == "face":
                        tile.face.set(self.face_frame.face_var.get())

        self.face_frame.face_var.trace("w", face_var_trace)

        # Create a frame for the wait menu
        self.wait_frame = tk.Frame(self, bg="#ffdc7d")
        self.wait_frame.grid(row=0, column=0, sticky="NESW")

        # menu title
        self.wait_frame.label = tk.Label(self.wait_frame, text="Wait Command", font="18", bg="#ffd21c")
        self.wait_frame.label.pack(anchor="n", fill="x")

        # a label and slider for the wait duration for the selected tile
        self.wait_frame.wait_label = tk.Label(self.wait_frame, text="\n\nDuration:", font="16", bg="#ffdc7d")
        self.wait_frame.wait_label.pack(anchor="center")

        self.wait_frame.wait_slider = tk.Scale(self.wait_frame, from_=500, to=5000, orient=tk.HORIZONTAL, width=30,
                                               length=150, bg="#ffdc7d", resolution=500)
        self.wait_frame.wait_slider.pack(anchor="center")

        # Create the default Menu to show on program start and tile clear
        self.tutorial_frame = tk.Frame(self, bg="#dbdbdb")
        self.tutorial_frame.grid(row=0, column=0, sticky="NESW")

        # menu title
        self.tutorial_frame.label = tk.Label(self.tutorial_frame, text="Welcome to FunBot!", font="20", bg="#ebebeb",
                                             wraplength=150)
        self.tutorial_frame.label.pack(anchor="n", fill="x")

        # Intro message
        self.tutorial_frame.message = tk.Label(self.tutorial_frame,
                                               text="\nDrag a command from the bottom tray into the command area to begin. Touch a command to edit its value. Press the Play button to start your program.\n",
                                               bg="#dbdbdb", wraplength=150)
        self.tutorial_frame.message.pack(anchor="center")

        # exit button, only way to stop program on a pi without a keyboard, ssh connection, or performing a force shutdown via removing power
        self.tutorial_frame.exit_button = tk.Button(self.tutorial_frame, text="Exit FunBot", command=self.quit)
        self.tutorial_frame.exit_button.pack(anchor="s")

        # Create the main canvas for animating buttons and actions
        self.main_frame = tk.Frame(self)
        self.main_frame.grid(row=0, column=1, sticky="NESW")
        self.main_canvas = tk.Canvas(self.main_frame, highlightthickness=0, bg="white")
        self.main_canvas.pack(fill="both", expand=True)

        # Create a list of command tiles for storing the current program and populate it with placeholder tiles
        self.tile_list = []
        for row in range(0, 4):
            for column in range(0, 5):
                self.tile_list.append(
                    action_tile(self.main_canvas, xpos=56 * column + 48, ypos=56 * row + 40, enabled=False))

        # Create and Show a clear button
        self.clear_tiles = action_tile(self.main_canvas, image="Resources/Clear.png", xpos=32, ypos=276, width=32,
                                       no_tile=True, enabled=False)

        def release_command():
            for target_tile in self.tile_list:
                target_tile.type = "null"
                target_tile.left_speed.set(75)
                target_tile.right_speed.set(75)
                target_tile.face.set("Happy")
                target_tile.wait.set(1500)
                target_tile.update_icon("Resources/Tile_Placeholder.png", target_tile.width.get(),
                                        target_tile.width.get())
            self.tutorial_frame.tkraise()

        self.clear_tiles.release_command = release_command

        # Create and Show the start and stop condition blocks
        self.start_tile = action_tile(self.main_canvas, image="Resources/Play.png", xpos=16, ypos=40, width=32,
                                      no_tile=True)
        self.start_tile.parent.tag_unbind(self.start_tile.icon_id, "<B1-Motion>")
        self.start_tile.release_command = self.run

        self.end_tile = action_tile(self.main_canvas, image="Resources/Stop.png", xpos=304, ypos=208, width=32,
                                    no_tile=True)
        self.end_tile.parent.tag_unbind(self.end_tile.icon_id, "<B1-Motion>")

        # the custom release command used to toggle the states of the end block from stop to loop and back
        def release_command():
            if self.end_tile.type == "loop":
                self.end_tile.update_icon("Resources/Stop.png", self.end_tile.width.get(), self.end_tile.width.get())
                self.end_tile.type = "stop"
            else:
                self.end_tile.update_icon("Resources/Loop.png", self.end_tile.width.get(), self.end_tile.width.get())
                self.end_tile.type = "loop"

        self.end_tile.release_command = release_command

        # Create and Show building block for drive commands
        self.drive_tile = action_tile(self.main_canvas, image="Resources/Forward_Straight.png", xpos=104, ypos=276)

        def drop_command():
            tile = self.drive_tile
            if tile.x.get() > 28 and tile.x.get() < 292 and tile.y.get() > 0 and tile.y.get() < 228:
                print("change")
                self.set_next_tile("drive")
            tile.x.set(104)
            tile.y.set(276)

        self.drive_tile.drop_command = drop_command

        # Create and Show building block for face commands
        self.face_tile = action_tile(self.main_canvas, image="Resources/Happy.png", xpos=160, ypos=276)

        def drop_command():
            tile = self.face_tile
            if tile.x.get() > 28 and tile.x.get() < 292 and tile.y.get() > 0 and tile.y.get() < 228:
                print("change")
                self.set_next_tile("face")
            tile.x.set(160)
            tile.y.set(276)

        self.face_tile.drop_command = drop_command

        # Create and Show building block for wait commands
        self.wait_tile = action_tile(self.main_canvas, image="Resources/Clock.png", xpos=216, ypos=276)

        def drop_command():
            tile = self.wait_tile
            if tile.x.get() > 28 and tile.x.get() < 292 and tile.y.get() > 0 and tile.y.get() < 228:
                print("change")
                self.set_next_tile("wait")
            tile.x.set(216)
            tile.y.set(276)

        self.wait_tile.drop_command = drop_command

        #################################################################
        # Data Collection for Analysis via Matlab                       #
        #     Program is unusable when this is enabled, as it captures  #
        #     all tkinter events for all widgets and canvas elements,   #
        #     rendering several components of the GUI innert            #
        #################################################################

        # this basically iterates through all tkinter elements, parents, and thier children and binds mouse events to them with the callback populating a dictionary of mouse event data
        self.mouse_event_list = []
        start_time = time.time()
        target_event_tags = ["<Button>",
                             "<ButtonRelease>",
                             "<Double-Button>",
                             "<Triple-Button>",
                             "<Enter>",
                             "<Leave>",
                             "<B1-Motion>",
                             "<B2-Motion>",
                             "<B3-Motion>",
                             "<B4-Motion>",
                             "<B5-Motion>"]

        def all_event_call(event):
            print(event)
            temp_dict = {}
            temp_dict["time"] = time.time() - start_time
            temp_dict["type"] = event.type
            # temp_dict["widget"] = event.widget
            # temp_dict["mouse_button"] = event.num
            temp_dict["x"] = event.x_root
            temp_dict["y"] = event.y_root
            self.mouse_event_list.append(temp_dict)

        def bind_to_all_children(parent):
            for child in parent.children.values():
                for event_tag in target_event_tags:
                    child.bind(event_tag, all_event_call)
                bind_to_all_children(child)
                print("bound all events to " + str(child))

        def capture_tkinter_events():
            for tile in self.tile_list:
                for event_tag in target_event_tags:
                    self.main_canvas.tag_bind(tile.tile_id, event_tag, all_event_call)
                    self.main_canvas.tag_bind(tile.icon_id, event_tag, all_event_call)
                print("bound all events to " + str(tile))
            for event_tag in target_event_tags:
                self.bind(event_tag, all_event_call)
                self.run_canvas.tag_bind(self.display_tile.icon_id, event_tag, all_event_call)
                self.main_canvas.tag_bind(self.end_tile.icon_id, event_tag, all_event_call)
                self.main_canvas.tag_bind(self.start_tile.icon_id, event_tag, all_event_call)
                self.main_canvas.tag_bind(self.drive_tile.icon_id, event_tag, all_event_call)
                self.main_canvas.tag_bind(self.face_tile.icon_id, event_tag, all_event_call)
                self.main_canvas.tag_bind(self.wait_tile.icon_id, event_tag, all_event_call)
                self.main_canvas.tag_bind(self.clear_tiles.icon_id, event_tag, all_event_call)
            print("bound all events to " + str(self))
            print("bound all events to " + str(self.display_tile))
            print("bound all events to " + str(self.end_tile))
            print("bound all events to " + str(self.start_tile))
            print("bound all events to " + str(self.drive_tile))
            print("bound all events to " + str(self.face_tile))
            print("bound all events to " + str(self.wait_tile))
            print("bound all events to " + str(self.clear_tiles))
            bind_to_all_children(self)

        # this dumps the data from the event log dictionary into a .csv file to be read from matlab
        def record_event_log():
            with open("tkinter_event_log.csv", "w") as csv_log:
                writer = csv.DictWriter(csv_log, fieldnames=["time", "type", "widget", "mouse_button", "x", "y"])
                # writer.writeheader()
                for current_event in self.mouse_event_list:
                    writer.writerow(current_event)
                    print(current_event)
                # writer.writerow(self.mouse_event_list[0])
                # for current_event in self.mouse_event_list:
                # print(current_event)
                # data_list = []
                # data_list.append(current_event["time"])
                # data_list.append(current_event["type"])
                # data_list.append(current_event["widget"])
                # data_list.append(current_event["mouse_button"])
                # data_list.append(current_event["x"])
                # data_list.append(current_event["y"])
                # writer.writerow(data_list)

        # Uncomment to enable event tracking, which will disable much of the GUI
        # capture_tkinter_events()
        # self.after(20000, record_event_log)

        ##############################################################

    # shows runtime GUI and starts the runtime process
    def run(self):
        print("run")
        self.run_frame.tkraise()
        self.perform_tile_command(0)

    # iterates through all impending commands in the tkinter.after buffer and cancels them
    def cancel_impending_commands(self, *args):
        for after_indstance in self.after_list:
            self.after_cancel(after_indstance)
        self.after_list = []
        self.tutorial_frame.tkraise()
        self.main_frame.tkraise()

    # performs the action specified by the currently active run-time tile and calls itself with the next tile to be active (is recursive)
    def perform_tile_command(self, tile_index):
        if tile_index >= len(self.tile_list):
            if self.end_tile.type == "loop":
                self.perform_tile_command(0)
                return
            else:
                print("stop")
                self.cancel_impending_commands()
                return
        active_tile = self.tile_list[tile_index]
        if active_tile.type == "wait":
            print("waiting for " + str(active_tile.wait.get()) + " milliseconds")
            self.after_list.append(
                self.drive_frame.after(active_tile.wait.get(), lambda i=tile_index + 1: self.perform_tile_command(i)))
        elif active_tile.type == "face":
            print("showing face " + active_tile.face.get())
            self.display_tile.update_icon("Resources/" + active_tile.face.get() + ".png", 0, 0)
            self.perform_tile_command(tile_index + 1)
        elif active_tile.type == "drive":
            print("driving at (" + str(active_tile.left_speed.get()) + "," + str(
                active_tile.right_speed.get()) + ") for " + str(active_tile.wait.get()) + " milliseconds")
            self.drive(active_tile.left_speed.get(), active_tile.right_speed.get(), duration=active_tile.wait.get())
            self.after_list.append(
                self.after(active_tile.wait.get(), lambda i=tile_index + 1: self.perform_tile_command(i)))
        elif active_tile.type == "null":
            self.perform_tile_command(tile_index + 1)

    # Finds the next unset tile in the tile_list and sets it to the specified type (used by command builders during drop events)
    def set_next_tile(self, type):
        target_tile = self.tile_list[0]
        for tile in self.tile_list:
            if tile.type == "null":
                target_tile = tile
                break
        target_tile.type = type
        if target_tile.type == "drive":
            target_tile.update_icon("Resources/Forward_Straight.png", target_tile.width.get(), target_tile.width.get())
        elif target_tile.type == "face":
            target_tile.update_icon("Resources/Happy.png", target_tile.width.get(), target_tile.width.get())
        elif target_tile.type == "wait":
            target_tile.update_icon("Resources/Clock.png", target_tile.width.get(), target_tile.width.get())

    # sets the duty cycle for both robot motors to 0, stopping the robot's motion
    def stop(self):
        if running_on_pi:
            pwm_A.ChangeDutyCycle(0)
            pwm_B.ChangeDutyCycle(0)

    # sets the duty cycle and direction for both robot motors according to the specified speeds, and stops motors after specified duration
    def drive(self, left_speed, right_speed, duration=1000):
        if running_on_pi:
            if left_speed > 0:
                # Drive the motor clockwise
                GPIO.output(38, GPIO.HIGH)  # Set AIN1
                GPIO.output(36, GPIO.LOW)  # Set AIN2
            else:
                # Drive the motor clockwise
                GPIO.output(38, GPIO.LOW)  # Set AIN1
                GPIO.output(36, GPIO.HIGH)  # Set AIN2
            if right_speed > 0:
                # Drive the motor counter-clockwise
                GPIO.output(35, GPIO.HIGH)  # Set BIN1
                GPIO.output(37, GPIO.LOW)  # Set BIN2
            else:
                # Drive the motor counter-clockwise
                GPIO.output(35, GPIO.LOW)  # Set BIN1
                GPIO.output(37, GPIO.HIGH)  # Set BIN2
            pwm_A.ChangeDutyCycle(abs(left_speed))
            pwm_B.ChangeDutyCycle(abs(right_speed))
            self.after(duration, self.stop)


# main script to run on startup
if __name__ == '__main__':
    # settup GPIO pins
    if running_on_pi:
        # Set GPIO pin settings
        GPIO.setmode(GPIO.BOARD)

        # set up A motor pins
        GPIO.setup(40, GPIO.OUT)  # speed control
        pwm_A = GPIO.PWM(40, 50)  # create pwm object
        pwm_A.start(0)  # start pwm_A at 0% duty cycle
        GPIO.setup(38, GPIO.OUT)  # dir control
        GPIO.setup(36, GPIO.OUT)  # dir control

        # set up B motor pins
        GPIO.setup(33, GPIO.OUT)  # speed control
        pwm_B = GPIO.PWM(33, 50)  # create pwm object
        pwm_B.start(0)  # start pwm_B at 0% duty cycle
        GPIO.setup(37, GPIO.OUT)  # dir control
        GPIO.setup(35, GPIO.OUT)  # dir control

    # create a tkinter window instance and load main_window object
    root = tk.Tk()
    my_gui = main_window(root)

    # start GUI frame loop
    root.mainloop()

    # after GUI has exited, lets cleanup the GPIO pins so we dont leave them active after shutting down
    if running_on_pi:
        # Reset all the GPIO pins by setting them to LOW
        GPIO.output(36, GPIO.LOW)  # Set AIN1
        GPIO.output(38, GPIO.LOW)  # Set AIN2
        GPIO.output(40, GPIO.LOW)  # Set PWMA
        GPIO.output(37, GPIO.LOW)  # Set AIN1
        GPIO.output(35, GPIO.LOW)  # Set AIN2
        GPIO.output(33, GPIO.LOW)  # Set PWMA
        # GPIO.output(13, GPIO.LOW) # Set STBY

        GPIO.cleanup()  # detaches from GPIO ports and tidies up objects

# end of program
