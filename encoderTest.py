import RPi.GPIO as GPIO
import numpy as np
import time
import multiprocessing
import logging
from multiprocessing import Process, Manager

# logging.getLogger().setLevel(logging.DEBUG)
logging.getLogger().setLevel(logging.INFO)

class Encoder:
    def __init__(self, pin1, pin2):
        self.pin1 = pin1
        self.pin2 = pin2
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(pin1, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(pin2, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

        manager = Manager()
        self.data = manager.dict()
        self.data['pos'] = 0
        self.data['absPos'] = 0
        self.data['speed'] = 0
        self.data['running'] = True

        self.process = Process(target=self.track, args=(self.data,))
        self.process.start()

    def stop(self):
        self.data['running'] = False

    def getPosition(self):
        return self.data['pos']

    def getAbsolutePosition(self):
        return self.data['absPos']

    def getSpeed(self):
        return self.data['speed']

    def track(self, data):
        averageTime = 0.25
        positionData = [[time.time(), 0]]
        last = [GPIO.input(self.pin1), GPIO.input(self.pin2)]
        while data['running']:
            current = [GPIO.input(self.pin1), GPIO.input(self.pin2)]
            isSame = current[0] == current[1]
            wasSame = last[0] == last[1]
            if wasSame and not isSame:
                forward = (last[0] == 0 and current[0] == 1) or (last[0] == 1 and current[0] == 0)
                if forward:
                    data['pos'] = (data['pos'] + 0.5 + 180) % 360 - 180
                    data['absPos'] += 0.5
                    logging.debug('F' + str(data['pos']))
                else:
                    data['pos'] = (data['pos'] - 0.5 + 180) % 360 - 180
                    data['absPos'] -= 0.5
                    logging.debug('R' +  str(data['pos']))
    
                positionData.append([time.time(), data['absPos']])
            for i in range(len(positionData)-1, -1, -1):
                if time.time() > positionData[i][0]+averageTime:
                    positionData = positionData[i+1:]
                    break
            if len(positionData) > 0:
                rate = (data['absPos']-positionData[0][1])/(time.time()-positionData[0][0])
                data['speed'] = rate
            last = current


# enc = Encoder(20, 21) # these are the pins I arbitrarily used
enc = Encoder(27, 17)


for i in range(10000):
    print('Position={:.1f} deg ({:.1f} absolute), Speed={:.2f} deg/s'.format(enc.getPosition(), enc.getAbsolutePosition(), enc.getSpeed()))
    time.sleep(0.1)



print('Done!')
GPIO.cleanup()
x