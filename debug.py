import numpy as np
import csv
import matplotlib.pyplot as plt
import time, os
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setup(20, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(21, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

startTime = time.time()

data = []
while time.time()-startTime < 30:
    data.append([time.time()-startTime, GPIO.input(20), GPIO.input(21)])

with open('data.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    writer.writerows(data)

print(len(data))
print(len(data)/30)
print('Done!')